def name(fullname):
    return fullname

def greetings(fullname):
    my_name = name(fullname)
    return f"Hello, {my_name}"


if __name__ == "__main__":
    print(greetings("B.L."))

